<?php


class HtmlProductListRenderer
{
    public function render($title, $products)
    {
        $priceFormatter = new HtmlPriceFormatter();

        $html = array_reduce(
            $products,
            function($html, $product) use ($priceFormatter) {
                return $html . sprintf(
                    '<li>[%s] %s - %s</li>',
                    $product['id'],
                    $product['name'],
                    $priceFormatter
                        ->format(
                            $product['price']
                        )
                );
            }
        );

        return sprintf(
            '<h2>%s</h2><ul>%s</ul>',
            $title,
            $html
        );
    }
}