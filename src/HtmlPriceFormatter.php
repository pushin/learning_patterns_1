<?php


class HtmlPriceFormatter
{
    public function format($value)
    {
        $template =
            '<span class="price">' .
                '<span class="whole">%u</span>.' .
                '<span class="fraction">%u</span> ' .
                '<span class="currency">%s</span>' .
            '</span>'
        ;

        list($whole, $fraction)
            = explode('.', $value)
        ;

        return sprintf(
            $template,
            $whole,
            $fraction,
            'р'
        );
    }
}