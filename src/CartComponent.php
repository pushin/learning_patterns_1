<?php


class CartComponent
{
    public function execute()
    {
        $db = new Database();
        $listRenderer = new HtmlProductListRenderer();

        echo $listRenderer
            ->render('Корзина', $db->fetchCartProducts()
        );
    }
}