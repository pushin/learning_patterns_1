<?php


class CatalogComponent
{
    public function execute()
    {
        $db = new Database();
        $listRenderer = new HtmlProductListRenderer();

        echo $listRenderer
            ->render('Каталог', $db->fetchProducts()
        );
    }
}