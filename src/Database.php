<?php


class Database
{
    public function __construct()
    {
        $this->connect();
    }

    public function connect()
    {

    }

    public function fetchProducts()
    {
        return [
            [
                'id' => 1,
                'name' => 'Cтол',
                'price' => 2340.56,
            ],
            [
                'id' => 2,
                'name' => 'Диван',
                'price' => 23140.56,
            ],
        ];
    }

    public function fetchCartProducts()
    {
        return [
            [
                'id' => 3,
                'name' => 'Кресло',
                'price' => 5300.99,
            ],
            [
                'id' => 4,
                'name' => 'Кровать',
                'price' => 11640.72,
            ],
        ];
    }
}